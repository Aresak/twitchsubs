package cz.symbiant.twitchsubs;

import java.io.*;

/**
 * Created by ares7 on 24.06.2016.
 */
public class InstaFile {
    public static void write(String path, String value) {
        path = escaped(path);
        try {
            prepare(path);
            FileWriter writer = new FileWriter(path, true);
            writer.write(value);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void rewrite(String path, String value) {
        delete(path);
        write(path, value);
    }

    public static String root() {
        try {
            return new File( "." ).getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String read(String path) {
        String s = "";
        path = escaped(path);
        try {
            FileReader reader = new FileReader(path);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                s += line;
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    public static void delete(String path) {
        path = escaped(path);
        (new File(path)).delete();
    }

    public static void prepare(String path) {
        path = escaped(path);
        File f = new File(path);
        String dirOnly = "";
        for(int i = 0; i < (path.split("/").length) - 1; i ++) {
            dirOnly += "/" + path.split("/")[i];
        }
        File dir = new File(dirOnly);

        if(!dir.exists())
            dir.mkdirs();
        f.setReadable(true);
        f.setWritable(true);
        if(!f.exists())
        {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String escaped(String path) {
        return path.replaceAll("#", "").replaceAll("@", "");
    }
}
