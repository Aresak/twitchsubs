package cz.symbiant.twitchsubs;



import java.util.LinkedList;

/**
 * Created by ares7 on 06.08.2016.
 */
public class SmartBlock {
    String name = "";
    LinkedList<SeamlessValue> properties = new LinkedList<SeamlessValue>();

    public String error;
    public boolean isNull;

    public SmartBlock(String blockName) {
        name = blockName;
    }
    public SmartBlock() {}

    public void addProperty(String name, String value) {
        properties.add(new SeamlessValue(name, value));
    }

    public String getProperty(String name) {
        for (SeamlessValue property : properties) {
            if(property.name.equals(name))
                return property.value;
        }
        System.out.println("SmartBlock Debug: Value " + name + " in SmartBlock " + name + " is unassigned");
        return "";
    }

    public SeamlessValue getValue(String name) {
        for (SeamlessValue property : properties) {
            if(property.name.equals(name))
                return property;
        }
        return null;
    }

    public void addProperty(String name, int value) {
        addProperty(name, Integer.toString(value));
    }

    public void addProperty(String name, long value) {
        addProperty(name, Long.toString(value));
    }

    public void addProperty(String name, float value) {
        addProperty(name, Float.toString(value));
    }

    public void addProperty(String name, double value) {
        addProperty(name, Double.toString(value));
    }

    public void addProperty(String name, boolean value) {
        addProperty(name, Boolean.toString(value));
    }

    public void setProperty(String name, String value) {
        properties.remove(getValue(name));
        addProperty(name, value);
    }

    public int getIntProperty(String name) {
        return Integer.parseInt(getProperty(name));
    }

    public long getLongProperty(String name) {
        return Long.parseLong(getProperty(name));
    }

    public float getFloatProperty(String name) {
        return Float.parseFloat(getProperty(name));
    }

    public double getDoubleProperty(String name) {
        return Double.parseDouble(getProperty(name));
    }

    public boolean getBoolProperty(String name) {
        return Boolean.parseBoolean(getProperty(name));
    }

    public SmartBlock process(String syntax) {
        SmartBlock smartBlock = new SmartBlock();

        char[] bar = syntax.toCharArray();
        int blocks = 0;

        boolean isProperty = false;
        boolean isValue = false;

        String currentPropertyName = "";
        String currentPropertyValue = "";

        String errorValue = "";

        for(char c : bar) {
            switch(c)
            {
                case '{':
                    // Start Block
                    blocks++;
                    break;
                case '}':
                    // End Block
                    blocks--;
                    if(errorValue.length() > 0)
                    {
                        // An Error has occured
                        if (errorValue.equals("null"))
                            isNull = true;
                        else
                            error = errorValue;
                    }
                    break;
                case '[':
                    // Property Start
                    isProperty = true;
                    break;
                case ']':
                    // Property End
                    // Build property
                    addProperty(convertFrom(currentPropertyName), convertFrom(currentPropertyValue));

                    currentPropertyName = "";
                    currentPropertyValue = "";

                    // WIPE Props
                    isProperty = false;
                    isValue = false;
                    break;
                case ':':
                    // An value is now comming on
                    if (isProperty) // If is an Property Value
                        isValue = true;
                    // else it's just the properties values !ignore!
                    break;
                default:
                    // An character/non-SB character
                    if(isProperty)
                    {
                        // The character is for the property use
                        if(isValue)
                        {
                            // The character is part of the property value
                            currentPropertyValue += c;
                        }
                        else
                        {
                            // The character is part of the property name
                            currentPropertyName += c;
                        }
                    }
                    else
                    {
                        if(isValue)
                        {
                            // Error message
                            errorValue += c;
                        }
                        else
                        {
                            // The character is the block name part
                            name += c;
                        }
                    }
                    break;
            }
        }

        return smartBlock;
    }

    public String process() {
        String result = "";
        result += "{"; // Open Block

        result += name + ":"; // Set Block Name

        if(properties != null) {
            for(int a = 0; a < properties.size(); a ++) {
                result += "["; // Open Property
                result += convertTo(properties.get(a).name) + ":" + convertTo(properties.get(a).value);
                result += "]"; // Close Property
            }
        }
        else {
            result += "null";
        }

        result += "}"; // Close Block
        return result;
    }

    public String convertTo(String r) {
        return r.replaceAll(":", "&colon;");
    }

    public String convertFrom(String r) {
        return r.replaceAll("&colon;", ":");
    }

    public void Debug() {
        System.out.println("SmartBlock Debug: ==/ SmartBlock \\==");

        System.out.println("SmartBlock Debug: Name: " + name);
        try {
            for(SeamlessValue property : properties) {
                if(property == null) {
                    System.out.println("SmartBlock Debug: Null property");
                    continue;
                }
                System.out.println("SmartBlock Debug: Property " + property.name + ": " + property.value);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        System.out.println("SmartBlock Debug: ==/ SmartBlock \\==");
    }
}
