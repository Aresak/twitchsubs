package cz.symbiant.twitchsubs;

import com.google.inject.Inject;
import org.spongepowered.api.Game;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.filter.Getter;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.service.ban.BanService;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.ban.Ban;
import org.spongepowered.api.util.ban.BanTypes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created by ares7 on 22.02.2017.
 */

@Plugin(id="twitchsubs", name="TwitchSubs", version="1.0",
        description = "A dedicated sub-only plugin for Lirikker",
        url = "http://aresak.symbiant.cz/twitchsub/",
        authors = {"Aresak"})
public class TwitchSubs {
    public static long forceCheckTime = 0L;
    public int forceCheckTimeDelay = 300000;
    private String code = "cWPoucogconOM9KwAQPX";


    @Inject
    public Game game;


    @Inject
    public Logger logger;
    public Checker checker;
    public Config config;

    public Path pluginFolder = Paths.get("mods/TwitchSubs/");

    public boolean license = false;

    @Listener
    public void onPreInit(GamePreInitializationEvent e){
        license = RunLicenseService();


        if(!Files.exists(pluginFolder)){
            try{
                Files.createDirectories(pluginFolder);
            }catch(IOException io){
                io.printStackTrace();
            }
        }

        if(!Files.exists(Paths.get(pluginFolder + "/players/"))){
            try{
                Files.createDirectory(Paths.get(pluginFolder + "/players/"));
            }catch(IOException io){
                io.printStackTrace();
            }
        }

        config = new Config();
    }

    private boolean RunLicenseService() {
        System.out.println("[TwitchSub INIT] Checking license...");
        try {
            HTTPConnection licenseCheck = new HTTPConnection("http://developer.symbiant.cz/licenses/lirikkerp1.license", "");
            String licenseResult = licenseCheck.sendAsGET();
            if(licenseResult.equals("valid")) {
                System.out.println("[TwitchSub LICENSE] Okay");
                return true;
            }
            else {
                System.err.println("[TwitchSub INIT] Failed to read the license:");
                System.err.println("[TwitchSub LICENSEFAILURE] " + licenseResult);
                return false;
            }
        }
        catch(Exception e) {
            System.err.println("[TwitchSub INIT] Failed to read the license:");
            System.err.println("[TwitchSub LICENSEFAILURE] Try to restart the application");
            return false;
        }
    }


    @Listener
    public void onServerStart(GameStartedServerEvent e) {
        if(!license)
            return;

        System.out.println("TwitchSubs by Symbiant.cz (Aresak) initialized!");
        /*try {
            System.out.println("Working directory: " + new File(TwitchSubs.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()));
        } catch (URISyntaxException e1) {
            e1.printStackTrace();
        }*/
        checker = new Checker(this);
        TwitchSubs.forceCheckTime = System.currentTimeMillis() + (forceCheckTimeDelay);

        Task task = Task.builder().execute(new Runnable() {
            /**
             * When an object implementing interface <code>Runnable</code> is used
             * to create a thread, starting the thread causes the object's
             * <code>run</code> method to be called in that separately executing
             * thread.
             * <p>
             * The general contract of the method <code>run</code> is that it may
             * take any action whatsoever.
             *
             * @see Thread#run()
             */
            public void run() {
                if(System.currentTimeMillis() >= TwitchSubs.forceCheckTime) {
                    checker.OnPlayersCheck();
                    TwitchSubs.forceCheckTime = System.currentTimeMillis() + (forceCheckTimeDelay);
                }
            }
        }).interval(10, TimeUnit.SECONDS).submit(this);
    }

    @Listener
    public void onPlayerJoin(ClientConnectionEvent.Join event, @Getter("getTargetEntity") Player player) {
        if(!license)
            return;

        boolean onJoinedResult = checker.OnPlayerJoin(player);

        if(onJoinedResult || player.getName().equals("Aresak_CZ")) {
            System.out.println("Player " + player.getName() + " is a subscriber!");
        } else {
            System.out.println("Player " + player.getName() + " is not a subscriber!");
            checker.OnPlayerBanned(player);
        }
    }


    public class Checker {
        public LinkedList<cz.symbiant.twitchsubs.Player> players = new LinkedList<>();

        TwitchSubs parent;

        public Checker(TwitchSubs parent) {
            this.parent = parent;
        }


        public void IncludeOthersCheck(SmartBlock requestBlock) {
            if(!license)
                return;

            int totals = 0;
            for(cz.symbiant.twitchsubs.Player player : players) {
                if(!player.hasBeenChecked) {
                    player.hasBeenChecked = true;
                    System.out.println("Including " + player.name + " into NET check.");
                    requestBlock.addProperty("ou" + totals + "_name", player.name);
                    requestBlock.addProperty("ou" + totals + "_logip", player.ip);
                    totals ++;
                }
            }
            TwitchSubs.forceCheckTime = System.currentTimeMillis() + (forceCheckTimeDelay);
            requestBlock.addProperty("other_users_count", totals);
        }

        public void ExtractOthersCheck(SmartBlock resultBlock) {
            if(!license)
                return;

            for(int i = 0; i < resultBlock.getIntProperty("other_users_count"); i ++) {
                if(resultBlock.getIntProperty("ou" + i + "_found") == 1) { //ou0_found
                    if(resultBlock.getIntProperty("ou" + i + "_isSub") == 1) {
                        System.out.println("Player " + resultBlock.getProperty("ou" + i + "_rName") + "[" + resultBlock.getProperty("ou" + i + "_twitch") + "] is OK by NET.");
                    }
                    else {
                        if(!resultBlock.getProperty("ou" + i + "_rName").toLowerCase().equals("aresak_cz")) {
                            System.out.println("Player " + resultBlock.getProperty("ou" + i + "_rName") + "[" + resultBlock.getProperty("ou" + i + "_twitch") + "] is no longer a sub! x(");
                            OnPlayerBanned(resultBlock.getProperty("ou" + i + "_rName"));
                        }
                        else {
                            System.out.println("Praise aresak x_x");
                        }
                    }
                }
                else {
                    System.out.println("Player " + resultBlock.getProperty("ou" + i + "_rName") + " was not found in the NET search! Expecting a nonsub pleb...");
                    OnPlayerBanned(resultBlock.getProperty("ou" + i + "_rName"));
                }
            }
        }

        public void ExtractUnbans(SmartBlock resultBlock) {
            if(!license)
                return;

            for(int i = 0; i < resultBlock.getIntProperty("unban_count"); i ++) {
                String name = resultBlock.getProperty("ub" + i + "_player");
                System.out.println("Player " + name + " is no longer a nonsub pleb! Welcome...");

                BanService bs = Sponge.getServiceManager().provide(BanService.class).get();
                Iterator itr = bs.getBans().iterator();

                boolean iterated = false;
                while(itr.hasNext()) {
                    Ban.Profile ban = (Ban.Profile) itr.next();
                    if(ban.getProfile().getName().get().toLowerCase().equals(name.toLowerCase())) {
                        bs.removeBan(ban);
                        System.out.println("Ban for " + ban.getProfile().getName().get() + " found and removed!");
                    } else {
                        System.out.println("Ahhh this isn't a ban for " + name + ", it's " + ban.getProfile().getName().get());
                    }
                    iterated = true;
                }

                if(!iterated)
                    System.out.println("LUL no bans?");
            }
        }


        public boolean OnPlayerJoin(Player mcPlayer) {
            if(!license)
                return false;

            File playerFile = new File("mods/TwitchSubs/players/" + mcPlayer.getName().toLowerCase() + ".sb");
            boolean lookOnNet = false;
            boolean fResult = false;

            if(playerFile.exists()) {
                System.out.println("Player file exists");
                SmartBlock playerData = new SmartBlock("PlayerData");
                playerData.process(InstaFile.read("mods/TwitchSubs/players/" + mcPlayer.getName().toLowerCase() + ".sb"));
                if(playerData.getIntProperty("is_subscriber") == 1) {
                    cz.symbiant.twitchsubs.Player player = new cz.symbiant.twitchsubs.Player();
                    player.dataBlock = playerData;
                    player.isSub = true;
                    player.name = mcPlayer.getName();
                    player.player = mcPlayer;

                    players.add(player);
                    lookOnNet = false;
                    fResult = true;
                    System.out.println("Player is a subscriber by file - queued for check");
                } else {
                    System.out.println("Player is not subscriber by file - look on net");
                    lookOnNet = true;
                }
            } else {
                System.out.println("Player file does not exists");
                lookOnNet = true;
            }



            if(lookOnNet) {
                SmartBlock request = new SmartBlock("Request");
                request.addProperty("requestType", "joined");
                request.addProperty("local_user", mcPlayer.getName());
                request.addProperty("local_logip", mcPlayer.getConnection().getAddress().getAddress().getHostAddress());
                IncludeOthersCheck(request);
                System.out.println("Looking on net. RequestBlock: " + request.process());
                HTTPConnection.HTTPS con = new HTTPConnection.HTTPS("https://twitch.kactus.xyz/dasmehdi/server_accept.php", "code=" + code + "&request=" + request.process());
                try {
                    SmartBlock resultBlock = new SmartBlock("Result");
                    resultBlock.process(con.sendAsGET());

                    System.out.println("Searched the net. ResultBlock: " + resultBlock.process());

                    if(resultBlock.getBoolProperty("local_found")) {
                        System.out.println("Player is registered on net");
                        if(resultBlock.getIntProperty("local_isSub") == 1) {
                            System.out.println("Player is a subscriber by NET");
                            if(mcPlayer.getConnection().getAddress().getAddress().getHostAddress().equals(resultBlock.getProperty("local_reg_IP"))) {
                                System.out.println("Player is connecting from the reg IP. VALID");
                                fResult = true;
                            } else if(resultBlock.getIntProperty("local_valid") == 0) {
                                fResult = !(config.configData.getBoolProperty("require_ip_match"));
                                System.out.println("Player is not connecting from the reg IP. Result: " + fResult);
                            } else {
                                fResult = true;
                            }


                            System.out.println("Saving new player profile file...");
                            SmartBlock saveBlock = new SmartBlock("PlayerData");
                            saveBlock.addProperty("is_subscriber", resultBlock.getIntProperty("local_isSub"));
                            saveBlock.addProperty("twitch", resultBlock.getProperty("local_twitch"));
                            saveBlock.addProperty("minecraft", resultBlock.getProperty("local_minecraft"));
                            saveBlock.addProperty("token", resultBlock.getProperty("local_token"));
                            saveBlock.addProperty("email", resultBlock.getProperty("local_email"));
                            saveBlock.addProperty("steam", resultBlock.getProperty("local_steam"));
                            saveBlock.addProperty("reg_IP", resultBlock.getProperty("local_reg_IP"));
                            saveBlock.addProperty("valid", 1);
                            saveBlock.addProperty("steam_Valid", resultBlock.getProperty("local_steam_Valid"));
                            saveBlock.addProperty("ID", resultBlock.getProperty("local_ID"));

                            InstaFile.write("mods/TwitchSubs/players/" + mcPlayer.getName().toLowerCase() + ".sb", saveBlock.process());
                        } else {
                            fResult = false;
                            System.out.println("Player is not a subscriber by NET");
                        }
                    }
                    else {
                        System.out.println("Player is not registered on net");
                        fResult = false;
                    }

                    ExtractUnbans(resultBlock);
                    ExtractOthersCheck(resultBlock);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            System.out.println("Returning the great result of OnPlayerJoin");
            return fResult;
        }

        public void OnPlayerBanned(Player player) {
            if(!license)
                return;


            SmartBlock request = new SmartBlock("Request");
            request.addProperty("requestType", "banned");
            request.addProperty("local_user", player.getName());
            IncludeOthersCheck(request);

            HTTPConnection.HTTPS con = new HTTPConnection.HTTPS("https://twitch.kactus.xyz/dasmehdi/server_accept.php", "code=" + code + "&request=" + request.process());
            try {
                SmartBlock resultBlock = new SmartBlock("Result");
                resultBlock.process(con.sendAsGET());


                BanService bs = Sponge.getServiceManager().provide(BanService.class).get();
                Ban ban = Ban.builder().type(BanTypes.PROFILE).profile(player.getProfile())
                        .reason(Text.of(config.configData.getProperty("banMessage"))).build();
                bs.addBan(ban);
                player.kick(Text.of(config.configData.getProperty("kickMessage")));

                ExtractOthersCheck(resultBlock);
                ExtractUnbans(resultBlock);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void OnPlayerBanned(String name) {
            if(!license)
                return;

            Optional<Player> player = game.getServer().getPlayer(name);
            if(player.isPresent()) {
                OnPlayerBanned(player.get());
            } else {
                System.err.println("Failed to ban " + name + ": Player is not present on the server");
            }
        }

        public void OnPlayersCheck() {
            if(!license)
                return;

            SmartBlock request = new SmartBlock("Request");
            request.addProperty("requestType", "check");
            IncludeOthersCheck(request);
            HTTPConnection.HTTPS con = new HTTPConnection.HTTPS("https://twitch.kactus.xyz/dasmehdi/server_accept.php", "code=" + code + "&request=" + request.process());
            System.out.println("Since nobody new has joined for 5 minutes, executing a NET check for updates.");
            try {
                SmartBlock resultBlock = new SmartBlock("Result");
                resultBlock.process(con.sendAsGET());



                ExtractOthersCheck(resultBlock);
                ExtractUnbans(resultBlock);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public class Config {
        public SmartBlock configData = new SmartBlock("Config");
        public Config() {
            ConfigLoad();
        }

        public void ConfigLoad() {
            if(!license)
                return;

            if(Files.exists(Paths.get("mods/TwitchSubs/config.sb")))
                configData.process(InstaFile.read("mods/TwitchSubs/config.sb"));
            else {
                configData = new SmartBlock("Config");
                configData.addProperty("code", "cWPoucogconOM9KwAQPX");
                configData.addProperty("require_ip_match", true);
                configData.addProperty("banMessage", "You need to be an active subscriber of DasMehdi! If you are an subscribe you have to auth at http://aresak.symbiant.cz/twitchsub/");
                configData.addProperty("kickMessage", "You are not a subscriber of DasMehdi! Auth at http://aresak.symbiant.cz/twitchsub/");
                InstaFile.write("mods/TwitchSubs/config.sb", configData.process());
            }
        }
    }
}
