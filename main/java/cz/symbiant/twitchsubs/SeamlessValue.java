package cz.symbiant.twitchsubs;

/**
 * Created by ares7 on 11.08.2016.
 */
public class SeamlessValue {
    public String name;
    public String value;

    public SeamlessValue(String name, String value)
    {
        this.name = name;
        this.value = value;
    }
}
