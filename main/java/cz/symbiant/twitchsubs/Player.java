package cz.symbiant.twitchsubs;

/**
 * Created by ares7 on 04.03.2017.
 */
public class Player {
    public String name = "";
    public String ip = "";
    public boolean isSub = false;
    public boolean hasBeenChecked = false;
    public org.spongepowered.api.entity.living.player.Player player = null;

    public SmartBlock dataBlock = null;
}
